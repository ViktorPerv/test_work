<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181125_102945_payments_create
 */
class m181125_102945_payments_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
	{
            
        $columns = array(
            'id'=>Schema::TYPE_PK,
            'uid'=>'integer NOT NULL',
            'created'=>'DATETIME NOT NULL DEFAULT NOW()',
            'operation'=>'integer NOT NULL',
            'type'=>'integer DEFAULT NULL',
            'sum'=>'decimal NOT NULL',
            'description'=>'text NOT NULL',
        );
        
        $this->createTable('payments', $columns);
        // creates index for column `uid`
        $this->createIndex(
            'idx-payments',
            'payments',
            'uid'
        );

        $this->addForeignKey('payments_uid_id_fk','payments','uid','user','id','CASCADE');
                    
	}

	public function safeDown()
	{
        $this->dropForeignKey('payments_uid_id_fk', 'payments');
        $this->dropTable('payments');
		return true;
	}
}
