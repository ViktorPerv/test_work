<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181126_194903_create_gift
 */
class m181126_194903_create_gift extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
	{
            
        $columns = array(
            'id'=>Schema::TYPE_PK,
            'name'=>'text NOT NULL',
            'created'=>'DATETIME NOT NULL DEFAULT NOW()',
            'type'=>'integer DEFAULT NULL',
            'allow_count'=>'integer DEFAULT NULL',
            'deleted'=>'integer DEFAULT 0',
        );
        
        $this->createTable('gifts', $columns);
        
            
	}

	public function safeDown()
	{
        $this->dropTable('gifts');
		return true;
	}
}
