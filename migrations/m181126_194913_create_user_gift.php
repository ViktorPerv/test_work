<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181126_194913_create_user_gift
 */
class m181126_194913_create_user_gift extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
	{
            
        $columns = array(
            'id'=> Schema::TYPE_PK,
            'created'=>'DATETIME NOT NULL DEFAULT NOW()',
            'uid'=>'integer NOT NULL',
            'gid'=>'integer NOT NULL',
            'deleted'=>'integer DEFAULT 0',
            'count'=>'integer DEFAULT 0',
        );
        
        $this->createTable('user_gifts', $columns);
        $this->createIndex(
            'uidx-user_gifts',
            'user_gifts',
            'uid'
        );
        $this->createIndex(
            'gidx-user_gifts',
            'user_gifts',
            'gid'
        );
        $this->addForeignKey('user_gift_uid_id_fk','user_gifts','uid','user','id','CASCADE');
        $this->addForeignKey('user_gift_gid_id_fk','user_gifts','gid','gifts','id','CASCADE');
            
	}

	public function safeDown()
	{
        $this->dropForeignKey('user_gift_uid_id_fk', 'user_gifts');
        $this->dropForeignKey('user_gift_gid_id_fk', 'user_gifts');
        $this->dropTable('user_gifts');
		return true;
	}
}
