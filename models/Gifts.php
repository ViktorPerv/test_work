<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gifts".
 *
 * @property int $id
 * @property string $name
 * @property string $created
 * @property int $type
 * @property int $allow_count
 * @property int $deleted
 *
 * @property UserGifts[] $userGifts
 */
class Gifts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gifts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['created'], 'safe'],
            [['type', 'allow_count', 'deleted'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created' => 'Created',
            'type' => 'Type',
            'allow_count' => 'Allow Count',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGifts()
    {
        return $this->hasMany(UserGifts::className(), ['gid' => 'id']);
    }
}
