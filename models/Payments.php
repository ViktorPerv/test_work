<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $uid                   id пользователя
 * @property string $created            дата создания
 * @property int $operation             операция (приход,расход)
 * @property int $type                  тип операции (для прихода)
 * @property string $sum                сумма операции
 * @property string $description
 *
 * @property User $u
 */
class Payments extends \yii\db\ActiveRecord
{
    
    /**
     * Тип операции - приход
     */
    const OPERATION_INCOME = 1;

    /**
     * Тип операции - расход
     */
    const OPERATION_EXPENSE = 2;

    /**
     * Тип прихода "Выиграл"
     */
    const TYPE_INCOME_PAYMENT = 1;

    /**
     * Тип прихода "Конвертация в бонусы"
     */
    const TYPE_INCOME_LOYALTY = 2;

     /**
     * Тип расхода "Конвертация в бонусы"
     */
    const TYPE_OUTCOME_LOYALTY = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid', 'operation', 'sum', 'description'], 'required'],
            ['operation', 'default', 'value' => self::OPERATION_INCOME, 'on' => ['income']],
            ['operation', 'default', 'value' => self::OPERATION_EXPENSE, 'on' => 'expense'],
            [['uid', 'operation', 'type'], 'integer'],
            [['created'], 'safe'],
            [['sum'], 'number'],
            [['description'], 'string'],
            [['uid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['uid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'created' => 'Created',
            'operation' => 'Operation',
            'type' => 'Type',
            'sum' => 'Sum',
            'description' => 'Description',
        ];
    }

    public function scopes() {
        return array(
            'income'=>array(
                'condition'=>$this->getTableAlias().'.operation = :operation',
                'params'=>array(':operation'=>self::OPERATION_INCOME),
            ),
            'expense'=>array(
                'condition'=>$this->getTableAlias().'.operation = :operation',
                'params'=>array(':operation'=>self::OPERATION_EXPENSE),
            ),
        );
    }

    /**
     * Условие выборки. Операции прихода.
     * @return \Payments
     */
    public function income() {

        $this->getDbCriteria()->mergeWith(array(
            'condition' => $this->getTableAlias() . '.operation=:operation',
            'params' => array(':operation' => self::OPERATION_INCOME),
        ));

        return $this;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'uid']);
    }

    /**
     * Получить список операций
     * @access public
     * @return array 
     */
    public function getOperationList() {

        return array(
            self::OPERATION_INCOME => 'Приход',
            self::OPERATION_EXPENSE => 'Расход',
        );
    }

    /**
     * Условие выборки. Операции текущего пользователя
     * @return \Payments
     */
    public function user() {

        $this->getDbCriteria()->mergeWith(array(
            'condition' => $this->getTableAlias() . '.uid=:uid',
            'params' => array(':uid' => Yii::app()->user->getId()),
        ));

        return $this;
    }
}
