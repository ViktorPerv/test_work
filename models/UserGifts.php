<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_gifts".
 *
 * @property int $id
 * @property string $created
 * @property int $uid
 * @property int $gid
 * @property int $deleted
 * @property int $count
 *
 * @property Gifts $g
 * @property User $u
 */
class UserGifts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_gifts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created'], 'safe'],
            [['uid', 'gid'], 'required'],
            [['uid', 'gid', 'deleted', 'count'], 'integer'],
            [['gid'], 'exist', 'skipOnError' => true, 'targetClass' => Gifts::className(), 'targetAttribute' => ['gid' => 'id']],
            [['uid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['uid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created' => 'Created',
            'uid' => 'Uid',
            'gid' => 'Gid',
            'deleted' => 'Deleted',
            'count' => 'Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGifts()
    {
        return $this->hasOne(Gifts::className(), ['id' => 'gid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'uid']);
    }
}
