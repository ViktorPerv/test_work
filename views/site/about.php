<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Test';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">

    <div class="row">
        <div class="col-md-8">
            <h2>
                Выбор подарка
            </h2>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <span class="game-box">
                            
                            Money
                            <label>0/1000</label>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <span class="game-box">
                            Points <label>0/?</label>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <span class="game-box">
                            Gifts <label>0/4    </label>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="height: 280px;">
                <div class="start-button" >
                    <?= Html::button('Старт', [
                        'class' => 'btn btn-success',
                        'style' => 'padding: 10px 40px;'
                        ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12 user-interface-top">
                    <h2>
                        Личный счет
                    </h2>
                </div>
            </div>
            <div class="user-interface-body">
                <div class="row">
                    <div class="col-md-12">
                        <span>
                            Money: 0$
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <span>
                            Points: 0
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <span>
                            Gifts: 0
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
